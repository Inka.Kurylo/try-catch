'use strict';
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  let body = document.querySelector('body');
  let div = document.createElement('div');
  div.id = "root";
  body.append(div);

  class NewError extends Error {
    constructor(message){
    super();
        this.name = 'NewError';
        this.message = message;
}
  }


  class CreatElem {
    constructor({author,name,price}){
        if  (!name){
            throw new NewError (`not value name`);
        } else if  (!author){
            throw new NewError (`not value author`);
        } else if  (!price){
            throw new NewError (`not value price`);
        } else {
            this.li = document.createElement('li');
            this.name = name;
            this.author = author;
            this.price = price;
        }
     }
    
    render(ul){
        this.li.innerText = `"${this.name}" ${this.author} ${this.price} грн`;
        ul.append(this.li);
    }

}

creatList(div,books);

function creatList(div,arr){
    let ul = document.createElement('ul'); 
   
    arr.forEach(el =>{
        try{
            new CreatElem(el).render(ul);
        }
            catch (e){
                if (e.name === "NewError" ){
                    console.warn(e);
        
                } else {
                    /* throw e; */
                }
        }
    });
div.append(ul);
}
